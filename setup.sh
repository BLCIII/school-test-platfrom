#!/bin/bash

user="quizzer"
password="testpassword"
database="quizdb"

max_username_len=50
max_quizname_len=80
max_question_len=512
max_answer_len=512
hash_len=64
class_len=4
perm_len=4
closed_question_points=1
open_question_points=3

mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_username_len/${max_username_len}/g; \
	s/%hash_len/${hash_len}/g; \
	s/%class_len/${class_len}/g; \
	s/%perm_len/${perm_len}/g;" \
	sql/tables/users.sql)
echo "Utworzono tabelę użytkowników"

mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_username_len/${max_username_len}/g; \
	s/%max_quizname_len/${max_quizname_len}/g; \
	s/%class_len/${class_len}/g;" \
	sql/tables/quizes.sql)
echo "utworzono tabelę quizów"

mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_question_len/${max_question_len}/g;" \
	sql/tables/questions.sql)
echo "Utworzono tabelę pytań"

mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_answer_len/${max_answer_len}/g;" \
	sql/tables/answers.sql)
echo "Utworzono tabelę odpowiedzi"

mysql -u $user -p${password} -D $database < sql/tables/scores.sql
echo "Utworzono tabelę wyników"
mysql -u $user -p${password} -D $database < sql/tables/relations.sql
echo "Instalacja tabel SQL zakończona"


mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_username_len/${max_username_len}/g; \
	s/%hash_len/${hash_len}/g;" \
	sql/procedures/users/auth.sql)
echo "Utworzono procedury weryfikacji"

mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_username_len/${max_username_len}/g; \
	s/%hash_len/${hash_len}/g; \
	s/%class_len/${class_len}/g;" \
	sql/procedures/users/newuser.sql)
echo "newuser.sql"

mysql -u $user -p${password} -D $database < sql/procedures/quiz/delquiz.sql
echo "delquiz.sql"

mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_username_len/${max_username_len}/g; \
	s/%class_len/${class_len}/g; \
	s/%max_quizname_len/${max_quizname_len}/g;" \
	sql/procedures/quiz/addquiz.sql)
echo "addquiz.sql"

mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_question_len/${max_question_len}/g; \
	s/%open_question_points/${open_question_points}/g; \
	s/%closed_question_points/${closed_question_points}/g;" \
	sql/procedures/quiz/addquestion.sql)
echo "addquestion.sql"
