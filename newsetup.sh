#!/bin/bash

user="quizzer"
password="testpassword"
database="quizdb"

max_username_len=50
max_quizname_len=80
max_question_len=512
max_answer_len=512
hash_len=64
class_len=4
perm_len=4
closed_question_points=1
open_question_points=3

function prep_and_exec_sql() {
	mysql -u $user -p${password} -D $database < <(sed " \
	s/%max_username_len/${max_username_len}/g; \
	s/%max_quizname_len/${max_quizname_len}/g; \
	s/%max_question_len/${max_question_len}/g; \
	s/%max_answer_len/${max_answer_len}/g; \
	s/%hash_len/${hash_len}/g; \
	s/%class_len/${class_len}/g; \
	s/%perm_len/${perm_len}/g; \
	s/%closed_question_points/${closed_question_points}/g; \
	s/%open_question_points/${open_question_points}/g; \
	" "$1")
}

level=0
function recurse_install() {
	for file in "$@" ; do
		if [[ -f "$file" ]] ; then
			#regular file
			prep_and_exec_sql "$file"
			echo "[${level}]executed $file"
		elif [[ -d "$file" ]] ; then
			#directory
			echo "[${level}]entering ${file}"
			level=$(( ++level ))
			#purely cosmetical
			dir_name="$file"
			recurse_install "${file}/"*
			level=$(( --level ))
			echo "[${level}]leaving ${dir_name}"
		fi
	done
}

recurse_install sql/*
