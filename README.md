## Platforma testowa/sprawdzianowa na licencji AGPL

Testowane na:  
* PHP PHP 7.0.5 (cli) (built: Apr  2 2016 23:10:23) ( NTS )
  * Zend Engine v3.0.0
* MySQL MariaDB Ver 10.1.13-MariaDB for Linux on x86_64 (MariaDB Server)
  * MySQL klient Ver 15.1 Distrib 10.1.13-MariaDB, for Linux (x86_64) using readline 5.1
* Arch Linux 4.5.0-1-ARCH
