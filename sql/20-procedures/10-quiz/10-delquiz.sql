/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

DELIMITER $

DROP PROCEDURE IF EXISTS DeleteQuiz$

CREATE PROCEDURE DeleteQuiz
	(IN param_user_id INT UNSIGNED,
	 IN param_quiz_id INT UNSIGNED)
	DETERMINISTIC
	MODIFIES SQL DATA
	COMMENT 'Delete a quiz. Requires \'d\' permission'
	BEGIN
	CALL AuthAction(param_user_id, 'd');
	START TRANSACTION READ WRITE;
		DELETE FROM `quizes` WHERE `quiz_id` = param_quiz_id;
		-- questions & answers are delete via foreign key cascade action
	COMMIT;
	END$

DELIMITER ;
