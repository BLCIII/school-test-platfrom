/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

DELIMITER $

DROP PROCEDURE IF EXISTS AddAnswer$

CREATE PROCEDURE AddAnswer
	(IN param_user_id INT UNSIGNED,
	 IN param_question_id INT UNSIGNED,
	 IN param_answer_txt VARCHAR(%max_answer_len) )
	DETERMINISTIC
	MODIFIES SQL DATA
	COMMENT 'Add answer to the quiz. Requires \'w\'permission'
	BEGIN
	CALL AuthAction(param_user_id, 'w');
	START TRANSACTION READ WRITE;
		INSERT INTO `answers` (`answer_parent_question`, `answer_content`)
			VALUES (param_question_id, param_answer_txt);
		-- return new answer ID
		SELECT LAST_INSERT_ID() AS `answer_id`;
	COMMIT;
	END$

DELIMITER $

DROP PROCEDURE IF EXISTS DeleteAnswer$

CREATE PROCEDURE DeleteAnswer
	(IN param_user_id INT UNSIGNED,
	 IN param_answer_id INT UNSIGNED)
	DETERMINISTIC
	MODIFIES SQL DATA
	COMMENT 'Remove answer from the quiz. Requires \'w\'permission'
	BEGIN
	CALL AuthAction(param_user_id, 'w');
	START TRANSACTION READ WRITE;
		DELETE FROM `answers` WHERE `answer_id` = param_answer_id;
	COMMIT;
	END$

DELIMITER $

DROP PROCEDURE IF EXISTS ChangeRightAnswer$

CREATE PROCEDURE ChangeRightAnswer
	(IN param_user_id INT UNSIGNED,
	 IN param_question_id INT UNSIGNED,
	 IN	param_answer_id INT UNSIGNED)
	DETERMINISTIC
	MODIFIES SQL DATA
	COMMENT 'Change right answer to the question. Requires \'w\'permission'
	BEGIN
	CALL AuthAction(param_user_id, 'w');
	START TRANSACTION READ WRITE;
		UPDATE `questions` SET
			`question_right_answer_id` = param_answer_id
			WHERE `question_id` = param_question_id;
	COMMIT;
	END$

DELIMITER ;
