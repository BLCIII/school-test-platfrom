/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

DELIMITER $

DROP PROCEDURE IF EXISTS AddQuestion$

CREATE PROCEDURE AddQuestion
	(IN param_user_id INT UNSIGNED,
	 IN param_quiz_id INT UNSIGNED,
	 IN param_question_is_open BOOLEAN,
	 IN param_question_content VARCHAR(%max_question_len),
  	 IN param_time_limit INT UNSIGNED)
	DETERMINISTIC
	MODIFIES SQL DATA
	COMMENT 'Add a question to the quiz. Requires \'w\' permission'
	BEGIN
	DECLARE question_points INT UNSIGNED;
	CALL AuthAction(param_user_id, 'w');
	START TRANSACTION READ WRITE;
		INSERT INTO `questions` (`question_content`, `question_parent_quiz`, `question_is_open`, `question_max_time_allowance`)
			VALUES (param_question_content, param_quiz_id, param_question_is_open, param_time_limit);
		IF param_question_is_open = TRUE THEN
			SET question_points = %open_question_points;
		ELSE
			SET question_points = %closed_question_points;
		END IF;
		UPDATE `quizes` SET
			`quiz_max_score` = `quiz_max_score` + question_points
			WHERE `quiz_id` = param_quiz_id;
		-- return new question ID
		SELECT LAST_INSERT_ID() AS `question_id`;
	COMMIT;
	END$

DELIMITER ;
