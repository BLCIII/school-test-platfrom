/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

DELIMITER $

DROP PROCEDURE IF EXISTS CreateEmptyQuiz$

CREATE PROCEDURE CreateEmptyQuiz
	(IN param_user_id INT UNSIGNED,
	 IN param_author_name VARCHAR(%max_username_len),
	 IN param_category INT UNSIGNED,
	 IN param_quiz_name VARCHAR(%max_quizname_len),
	 IN param_time_limit INT UNSIGNED,
	 IN param_quiz_class CHAR(%class_len) )
	DETERMINISTIC
	MODIFIES SQL DATA
	COMMENT 'Creates empty quiz. Requires \'w\' permission'
	BEGIN
	CALL AuthAction(param_user_id, 'w');
	START TRANSACTION READ WRITE;
		INSERT INTO `quizes` (`quiz_author`, `quiz_category`, `quiz_name`, `quiz_max_time_allowance`, `quiz_class`)
			VALUES (param_author_name, param_category, param_quiz_name, param_time_limit, param_quiz_class);
		-- returns new quiz ID
		SELECT LAST_INSERT_ID() AS `quiz_id`;
	COMMIT;
	END$

DELIMITER ;
