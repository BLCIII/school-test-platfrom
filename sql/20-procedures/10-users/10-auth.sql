/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

DELIMITER $

DROP PROCEDURE IF EXISTS AuthUser$

CREATE PROCEDURE AuthUser
	(IN param_user_name VARCHAR(%max_username_len),
	 IN param_challenge_hash BINARY(%hash_len) )
	 -- OUT out_status INT UNSIGNED )
	DETERMINISTIC
	READS SQL DATA
	COMMENT 'Authenticates the user, returns NULL on failure, user_id on success'
	BEGIN
	/*
	DECLARE EXIT HANDLER FOR SQLSTATE VALUE '02000' BEGIN
		/* no data error, aka user not autheticated *//*
		SET out_status = 0;
	END;
	*/
		-- SELECT `user_id` INTO out_status FROM `users`
		-- return both user id and permissions
		SELECT `user_id` AS `UID`, `user_permissions` AS `perms`
			FROM `users` WHERE `user_name` = param_user_name
			AND `user_password_hash` = param_challenge_hash;
		-- return userid if autheticated or nothing if not
	END$

DROP PROCEDURE IF EXISTS AuthAction$

CREATE PROCEDURE AuthAction
	(IN param_user_id INT UNSIGNED,
	 IN param_action CHAR(1) )
	DETERMINISTIC
	READS SQL DATA
	COMMENT 'Check if user has sufficient privileges for the action'
	BEGIN
		IF (SELECT INSTR(
			(SELECT `user_permissions` FROM `users` WHERE `user_id` = param_user_id),
			param_action) ) = 0 THEN
			-- insufficient privileges
			SIGNAL SQLSTATE VALUE '45000' SET
			MESSAGE_TEXT = 'insufficient privileges to perform this action';
		END IF;
		-- if privileges sufficient, do nothing
	END$

DELIMITER ;
