/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

DELIMITER $

DROP PROCEDURE IF EXISTS NewUser$

CREATE PROCEDURE NewUser
	(IN param_user_name VARCHAR(%max_username_len),
	 IN param_passwd_hash BINARY(%hash_len),
	 IN param_user_class CHAR(%class_len) )
	-- OUT out_user_id INT UNSIGNED)
	DETERMINISTIC
	MODIFIES SQL DATA
	COMMENT 'Create new user account with minimal (read) privileges'
	BEGIN
	START TRANSACTION READ WRITE;
		INSERT INTO `users` (`user_name`, `user_password_hash`, `user_class`, `user_permissions`)
			VALUES (param_user_name, param_passwd_hash, param_user_class, 'r---');
		-- SELECT LAST_INSERT_ID() INTO out_user_id;
		-- returns new user ID
		SELECT LAST_INSERT_ID() AS `UID`;
	COMMIT;
	END$

DELIMITER ;
