/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

DELIMITER $

DROP PROCEDURE IF EXISTS ChangeUserPermissions$

CREATE PROCEDURE ChangeUserPermissions
	(IN param_acting_user_id INT UNSIGNED,
	 IN param_changee_user_id INT UNSIGNED,
	 IN param_perm_mask CHAR(%perm_len) )
	DETERMINISTIC
	MODIFIES SQL DATA
	COMMENT 'Change permissions of some user account. Requires \'a\' permission'
	BEGIN
	CALL AuthAction(param_acting_user_id, 'a');
	START TRANSACTION READ WRITE;
		UPDATE `users` SET
			`user_permissions` = param_perm_mask
			WHERE `user_id` = param_changee_user_id;
	COMMIT;
	END$

DELIMITER ;
