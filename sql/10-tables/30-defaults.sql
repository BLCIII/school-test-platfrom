/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

INSERT INTO `users`
	(`user_name`, `user_password_hash`, `user_permissions`)
	VALUES ('admin', 'test', 'rwda');

INSERT INTO `quizes` VALUES
	('', 'testauthor', LAST_INSERT_ID(), 0, 'placeholder', 0, '', ' ', '', '');

SELECT LAST_INSERT_ID() INTO @var_quiz_id;

INSERT INTO `questions` VALUES
	('', 'Przykładowe zamknięte pytanie', @var_quiz_id, FALSE, NULL, 0),
	('', 'Przykładowe otwarte pytanie', @var_quiz_id, TRUE, NULL, 0);

SELECT LAST_INSERT_ID() INTO @var_question_id;

INSERT INTO `answers` VALUES
	('', @var_question_id, 'przykładowa odpowiedź A'),
	('', @var_question_id, 'przykładowa odpowiedź B'),
	('', @var_question_id, 'przykładowa odpowiedź C'),
	('', @var_question_id, 'przykładowa odpowiedź D');
