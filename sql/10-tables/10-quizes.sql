/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

SET SESSION FOREIGN_KEY_CHECKS = 0;
CREATE OR REPLACE TABLE `quizes` (
	`quiz_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`quiz_author` VARCHAR(%max_username_len),
	`quiz_adder` INT UNSIGNED,
	`quiz_category` INT UNSIGNED NOT NULL,
	`quiz_name` VARCHAR(%max_quizname_len) NOT NULL,
	`quiz_max_time_allowance` INT UNSIGNED NOT NULL DEFAULT 0, /*in seconds*/
	`quiz_max_score` INT UNSIGNED NOT NULL DEFAULT 0,
	`quiz_class` CHAR(%class_len),
	`quiz_no_questions` INT UNSIGNED DEFAULT 0,
	`quiz_creation_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`quiz_id`),
	CONSTRAINT UQ_quiz_names UNIQUE KEY (`quiz_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET SESSION FOREIGN_KEY_CHECKS = 1;
