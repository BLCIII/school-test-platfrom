/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

CREATE OR REPLACE TRIGGER T_no_quiz_questions_inc
	AFTER INSERT ON `questions` FOR EACH ROW
		UPDATE `quizes` SET
			`quiz_no_questions` = `quiz_no_questions` + 1
			WHERE `quizes`.`quiz_id` = NEW.`question_parent_quiz`;

CREATE OR REPLACE TRIGGER T_no_quiz_questions_dec
	BEFORE DELETE ON `questions` FOR EACH ROW
		UPDATE `quizes` SET
			`quiz_no_questions` = `quiz_no_questions` - 1
			WHERE `quizes`.`quiz_id` = OLD.`question_parent_quiz`;
