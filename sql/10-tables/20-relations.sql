/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

ALTER TABLE `quizes`
	ADD CONSTRAINT FK_quizes_to_users FOREIGN KEY (`quiz_adder`)
		REFERENCES users(user_id)
		ON UPDATE CASCADE ON DELETE SET NULL
;

ALTER TABLE `questions`
	ADD CONSTRAINT FK_questions_to_quizes
		FOREIGN KEY (`question_parent_quiz`)
		REFERENCES quizes(quiz_id)
		ON UPDATE CASCADE ON DELETE CASCADE,
	ADD CONSTRAINT FK_questions_to_correct_answers
		FOREIGN KEY (`question_right_answer_id`)
		REFERENCES answers(answer_id)
		ON UPDATE CASCADE ON DELETE CASCADE
;

ALTER TABLE `answers`
	ADD CONSTRAINT FK_answers_to_questions
		FOREIGN KEY (`answer_parent_question`)
		REFERENCES questions(question_id)
		ON UPDATE CASCADE ON DELETE CASCADE
;

ALTER TABLE `scores`
	ADD CONSTRAINT FK_scores_to_users FOREIGN KEY (`score_scoring_user`)
		REFERENCES users(user_id)
		ON UPDATE CASCADE ON DELETE CASCADE,
	ADD CONSTRAINT FK_scores_to_quizes FOREIGN KEY (`score_scored_quiz`)
		REFERENCES quizes(quiz_id)
		ON UPDATE CASCADE ON DELETE CASCADE
;
