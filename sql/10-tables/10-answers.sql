/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

SET SESSION FOREIGN_KEY_CHECKS = 0;
CREATE OR REPLACE TABLE `answers` (
	`answer_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`answer_parent_question` INT UNSIGNED NOT NULL,
	`answer_content` VARCHAR(%max_answer_len) NOT NULL,
	PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET SESSION FOREIGN_KEY_CHECKS = 1;
