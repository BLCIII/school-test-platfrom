function validateFormTime(e) {
	var timeel = $('input[name="time_allowance"]', e.target);
	var timestring = timeel.value;

	var hours = (timestring.indexOf('h') == -1) ?
		0 : (function () {
			var tmp = timestring.split('h');
			timestring = tmp[1];
			return parseInt(tmp[0], 10);
		})();

	var minutes = (timestring.indexOf('m') == -1) ?
		0 : (function () {
			var tmp = timestring.split('m');
			timestring = tmp[1];
			return parseInt(tmp[0], 10);
		})();

	var seconds = parseInt(timestring.split('s')[0], 10);
	seconds = isNaN(seconds) ? 0 : seconds;
	//convert time to seconds, as server expects
	timeel.value  = (hours * 3600) + (minutes * 60) + seconds;

	//return true so the form gets submitted
	return true;
}
