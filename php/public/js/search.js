function submitForm(e) {
	e.preventDefault();
	var query = $('input[name="search_query"]', e.target).value;
	var _class = $('select[name="search_class"]', e.target).value.toString();
	_class += $('select[name="search_subclass"]', e.target).value.toString();
	var body = ['query='+query, 'class='+_class].join('&');

	AJAX_search(body);
	return false;
}

function AJAX_search(query) {
    var xhr = new XMLHttpRequest();
	xhr.open('post', 'search.php');
	xhr.witCredentials = true;
	//xhr.setRequestHeader('Cookie: ', document.cookie);
    //without it PHP will fail to populate $_POST array
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send(encodeURI(query));
	xhr.onreadystatechange = function (e) {
		if (xhr.readyState != 4) return;
		$('#search_response').innerHTML = xhr.responseText;
	}
}

AJAX_search('query=&class=00');//make an empty query on page load
