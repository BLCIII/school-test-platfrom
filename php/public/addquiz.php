<?php
/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once('../session.php');
initSession();

if ($_SESSION['canwrite'] != true) {
	die('Niewystarczające uprawnienia aby odwiedźić tą stronę');
}

?>
<!DOCTYPE html5>
<html>
<head>
	<meta charset="utf-8">
	<title>Dodaj Test</title>
	<script type="text/javascript" src="js/selectors.js"></script>
	<script type="text/javascript" src="js/addquiz.js"></script>
</head>
<body>
<?php if (!isset($_POST['postquiz'])) { ?>
	<form action="" method="post" name="addquiz" onsubmit="validateFormTime(event);">
		Autor: <input type="text" name="author"></br>
		Kategoria: #TBI</br>
		Nazwa: <input type="text" name="quizname"></br>
		Limit czasowy: <input type="text" name="time_allowance"
			placeholder="np. 15m30s, 0 by wyłączyć"></br>
		Klasa: <select name="class">
				<option value="0">Dowolna</option>
				<option value="I">I</option>
				<option value="II">II</option>
				<option value="III">III</option>
			</select><select name="subclass">
				<option value="0">*</option>
				<option value="A">A</option>
				<option value="B">B</option>
				<option value="C">C</option>
				<option value="D">D</option>
				<option value="E">E</option>
			</select></br>
		<input type="submit" name="postquiz" value="Utwórz">
	</form>
<?php } /*end if (!isset($_POST['postquiz']) */ else {
	!empty($_POST['author']) or die('Podaj Autora');
	!empty($_POST['quizname']) or die('Podaj Nazwę');
	isset($_POST['time_allowance']) or die('Określ limit czasowy');

	require_once('../db_data.php');
	$link = initDBConn();

	$quiz_class = ($_POST['class'] == '0') ? '' : $link->real_escape_string($_POST['class']);
	$quiz_class .= ($_POST['subclass'] == '0') ? ' ' : $link->real_escape_string($_POST['subclass']);

	$result = callSQLProc("CreateEmptyQuiz('".$_SESSION['uid']
	."', '".$link->real_escape_string($_POST['author'])
	."', '0', '".$link->real_escape_string($_POST['quizname'])
	."', '".$link->real_escape_string($_POST['time_allowance'])
	."', '".$quiz_class."');");

	echo 'Utworzono.</br>
		<script>setTimeout(function ()
		{ window.location.assign("/editquiz.php?id='.$result['quiz_id'].'");
		}, 1000);</script>';
} /*end else if (!isset($_POST['postquiz']) */ ?>
</body>
</html>
