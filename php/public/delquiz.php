<?php
/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once('../session.php');
initSession();

$script_tag = '</br><script>setTimeout(function ()
	{ window.location.assign("index.php");
	}, 1000);</script>';

if ($_SESSION['candelete'] != true) {
	die('Brak wystarczających uprawnień aby wykonać akcję'.$script_tag);
} else {
	require_once('../db_data.php');
	$link = initDBConn();

	$quizid = $link->real_escape_string($_GET['id']);
	callSQLProc("DeleteQuiz('".$_SESSION['uid']."', '".$quizid."');");
	echo 'Usunięto test #'.$quizid.$script_tag;
}
