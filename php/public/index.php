<?php
/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once('../session.php');
initSession();
//$_SESSION['isadmin'] = $_SESSION['isadmin'] ?? false;
?>
<!DOCTYPE html5>
<html>
<head>
	<meta charset="utf-8">
	<title>Strona Główna</title>
	<script type="text/javascript" src="js/selectors.js"></script>
	<script type="text/javascript" src="js/search.js"></script>
	<link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<body>
	<!--Pomyślnie zalogowany</br></br>-->
	<div class="navbar"><nav><ul>
		<li>Zalogowany jako: <?= $_SESSION['username'] ?></li>
		<?php if ($_SESSION['isadmin'] == true) { ?>
		<li><a href="#">Admin</a></li>
			<!--dropdown menu-->
		<?php } /*end if ($_SESSION['isadmin'] == true) */ ?>
		<?php if ($_SESSION['canwrite'] == true) { ?>
		<li><a href="addquiz.php">Dodaj Test</a></li>
		<?php } /*end if ($_SESSION['canwrite'] == true) */ ?>
		<li><a href="#TBI">Moje wyniki</a></li>
		<li><a href="logout.php">Wyloguj</a></li>
	</ul></nav></div>
	</br>
	<?php if ($_SESSION['canread'] == true) { ?>
	Testy:</br>
	Szukaj: <form action="search.php" method="post" name="search" onsubmit="submitForm(event)">
		<input type="text" name="search_query" placeholder="Nazwa">
		</br>
	Kategoria: #TBI</br>
	Klasa: <select name="search_class">
			<option value="0" selected>Dowolna</option>
			<option value="I">I</option>
			<option value="II">II</option>
			<option value="III">III</option>
		</select><select name="search_subclass">
			<option value="0" selected></option>
			<option value="A">A</option>
			<option value="B">B</option>
			<option value="C">C</option>
			<option value="D">D</option>
			<option value="E">E</option>
		</select></br>
		<input type="submit" name="search_submit">
	</form></br>
	<div id="search_response"></div>
	<?php } /* end if ($_SESSION['canread'] == true) */ else { ?>
		Brak wystarczających uprawnień aby wyszukiwać i rozwiązywać testy</br>
		<?php } /* end else if ($_SESSION['canread'] == true) */ ?>

</br>
</br>
<hr>
<footer class="site-footer">
Opublikowane na licencji <a target="_blank" href="http://www.gnu.org/licenses/agpl.txt">AGPL</a>.</br>
Kod źródłowy dostępny <a target="_blank" href="https://gitlab.com/BLCIII/school-test-platfrom">tutaj</a>.
</footer>
</body>
</html>
