<?php
/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once('../errors.php');
require_once('../session.php');
initSession();
require_once('../db_data.php');
header('Content-type: text/plain');
$link = initDBConn();
$search_term = isset($_POST['query']) ?
	$link->real_escape_string($_POST['query']) : '';
$search_class = isset($_POST['class']) ?
	$link->real_escape_string((string)$_POST['class']) : '';
switch ((string)$search_class) {
	case '00':
	$search_pattern = '^.*$';
	break;
	case '0A':
	$search_pattern = '^I{0,3}A$';
	break;
	case '0B':
	$search_pattern = '^I{0,3}B$';
	break;
	case '0C':
	$search_pattern = '^I{0,3}C$';
	break;
	case '0D':
	$search_pattern = '^I{0,3}D$';
	break;
	case '0E':
	$search_pattern = '^I{0,3}E$';
	break;
	case 'I0':
	$search_pattern = 'I[^I]?$';
	break;
	case 'II0':
	$search_pattern = 'II[^I]?$';
	break;
	case 'III0':
	$search_pattern = 'III[^I]?$';
	break;
	default:
	$search_pattern = '^'.$search_class.'$';//perform exact match
	break;
}

$result = $link->query('SELECT `quiz_id`, `quiz_name`,
		`quiz_no_questions`, `quiz_category`, `quiz_class`
		FROM `quizes` WHERE `quiz_name` LIKE CONCAT(\'%\', \''
		.$search_term.'\', \'%\') AND `quiz_class` RLIKE \''
		.$search_pattern.'\';');
if ($result == false) {
	fatal_error(__FILE__, __LINE__, $link->error);
}
if ($result->num_rows > 0) {
	//no </tr> at the end, so it can be easily appended to
	$header_start = '<table><tr><th>Nazwa</th><th>Kategoria</th><th>Klasa</th><th>Liczba Pytań</th>';
	$writecol = '';
	$delcol = '';
	function row_start() {
		global $row;

		$qclass = empty($row['quiz_class']) ? '*' : $row['quiz_class'];
		return<<<EOT
<tr><td><a href="quiz.php?id={$row['quiz_id']}">{$row['quiz_name']}</a></td>
<td>{$row['quiz_category']}</td><td>{$qclass}</td>
<td>{$row['quiz_no_questions']}</td>
EOT;
//no </tr> for appending purposes
	}
	if ($_SESSION['canwrite'] === true) {
		$header_start .= '<th>Edytuj</th>';
		$writecol = function () {
			global $row;
			return '<td><a href="editquiz.php?id='.$row['quiz_id'].'">Edytuj</a></td>';
		};
	}
	if ($_SESSION['candelete'] === true) {
		$header_start .= '<th>Usuń</th>';
		$delcol = function () {
			global $row;
			return '<td><a href="delquiz.php?id='.$row['quiz_id'].'" onclick="return confirm(\'Jesteś pewien?\');">Usuń</a></td>';
		};
	}
	echo $header_start.'</tr>';
	while ($row = $result->fetch_assoc()) {
		echo row_start().$writecol().$delcol().'</tr>';
		}
	echo '</table>';
} else {
	echo 'Brak testów spełniających podane kryteria wyszukiwania';
}
