<?php
/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once('../session.php');
require_once('../errors.php');
initNonLoggedInSession();

if (!isset($_POST['submit'])
	|| !isset($_POST['nick'])
	|| !isset($_POST['pass'])
	) {
?>
<!DOCTYPE html5>
<html>
<head>
	<meta charset="utf-8">
	<title>Zarejestruj</title>
</head>
<body>
	Nowy Użytkownik</br>
	<form action="" method="post" name="log_in">
		Login: <input type="text" name="nick"></br>
		Hasło: <input type="password" name="pass"></br>
		Klasa: <select name="class">
				<option value="1">I</option>
				<option value="2">II</option>
				<option value="3" selected>III</option>
			</select><select name="subclass">
				<option value="A">A</option>
				<option value="B">B</option>
				<option value="C" selected>C</option>
				<option value="D">D</option>
				<option value="E">E</option>
			</select>
		<input type="submit" name="submit" value="Zarejestruj">
	</form>
</body>
</html>
<?php
} else {
	//fix class and subclass ranges
	if ($_POST['class'] > 3) $_POST['class'] = 3;
	if ($_POST['class'] < 1) $_POST['class'] = 1;
	if ($_POST['subclass'] > 69 || $_POST['subclass'] < 65)
		$_POST['subclass'] = 'C';
	$user_class = str_repeat("I", $_POST['class']);
	$user_class .= $_POST['subclass'];

	require_once('../db_data.php');
	$link = initDBConn();

	if ($link->multi_query("CALL NewUser('"
		.$link->real_escape_string($_POST['nick'])
		."', '"
		.$link->real_escape_string($_POST['pass'])
		."', '"
		.$user_class
		."');") == false) {
		//fatal_error(__FILE__, __LINE__, $link->error);
		die('Błąd lub nazwa użytkownika zajęta');
	}
	$result = $link->store_result();
	$row = $result->fetch_assoc();
	$_SESSION['uid'] = $row['UID'];
	$_SESSION['loggedin'] = true;
	//assume non admin by default
	$_SESSION['isadmin'] = false;
	header('HTTP/1.1 302 Found');
	header('Location: /index.php');
	header('Cache-Control: no-cache');
	while ($link->next_result());
	$result->free();
	$link->close();
}
