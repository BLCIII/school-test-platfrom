<?php
/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once('../session.php');
require_once('../errors.php');
//this would go into infinite loop
//initSession();
initNonLoggedInSession();

if (!isset($_POST['submit'])
	|| !isset($_POST['nick'])
	|| !isset($_POST['pass'])
	) {
	$_SESSION['csrf_token'] = rand();
?>
<!DOCTYPE html5>
<html>
<head>
	<meta charset="utf-8">
	<title>Log in</title>
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
	<?php
	if (isset($_SESSION['bad_login_msg'])) {
		echo '<span class="badlogin">'
			.$_SESSION['bad_login_msg']
			.'</span></br>';
	}
	?>
	<form action="" method="post" name="log_in">
		Login: <input type="text" name="nick"></br>
		Hasło: <input type="password" name="pass"></br>
		<input type="hidden" name="token"
			value="<?= $_SESSION['csrf_token'] ?>">
		<input type="submit" name="submit" value="Zaloguj">
	</form>
	</br>
	<a href="newuser.php">Nowy Użytkownik</a>
</body>
</html>
<?php
} else {
	if ($_POST['token'] != $_SESSION['csrf_token']) {
		die("Malformed form request\n"
			.'POST: '.$_POST['token']."\n"
			.'SESSION: '.$_SESSION['csrf_token']);
	}
	require_once('../db_data.php');
	$link = initDBConn();

	//stored procedures must be called with multi_query()
	if ($link->multi_query("CALL AuthUser('"
		.$link->real_escape_string($_POST['nick'])
		."', '"
		/* only simple (plaintext) password verification so far */
		.$link->real_escape_string($_POST['pass'])
		."');") == false) {
		fatal_error(__FILE__, __LINE__, $link->error);
	}
	$result = $link->store_result();
	if ($result->num_rows == 0) {
		//not authencicated
		//redirect to self
		//(aka redo the login procedure)
		$result->free();
		$link->close();
		$_SESSION['bad_login_msg'] = "Błędny login lub hasło";
		header('HTTP/1.1 302 Found');
		header('Location: /login.php');
		header('Cache-Control: no-cache');
		exit;
	} else {
		$row = $result->fetch_assoc();
		$_SESSION['uid'] = $row['UID'];
		$_SESSION['loggedin'] = true;
		$_SESSION['username'] = $link->real_escape_string($_POST['nick']);
		//$_SESSION['permissions'] = $row['perms'];
		$_SESSION['canread'] = ($row['perms'][0] == 'r') ? true : false;
		$_SESSION['canwrite'] = ($row['perms'][1] == 'w') ? true : false;
		$_SESSION['candelete'] = ($row['perms'][2] == 'd') ? true : false;
		$_SESSION['isadmin'] = ($row['perms'][3] == 'a') ? true : false;

		//logged in, redirect to index.php
		header('HTTP/1.1 302 Found');
		header('Location: /index.php');
		header('Cache-Control: no-cache');
	}
	$link->close();
}
