<?php
/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function initSession()
{
	initNonLoggedInSession();
	if ($_SESSION['loggedin'] != true) {
		header('HTTP/1.1 302 Found');
		header('Location: /login.php');
		header('Cache-Control: no-cache');
		exit;
	}
}

function initNonLoggedInSession()
{
	session_start();
	if ((!isset($_SESSION['wasinit']))
	|| ($_SESSION['ip'] != $_SERVER['REMOTE_ADDR'])) {
		session_regenerate_id();
		//anti session-fixation protection
		$_SESSION['wasinit'] = true;
		$_SESSION['loggedin'] = false;
		//anti session-hijacking protection
		$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
	}
}
