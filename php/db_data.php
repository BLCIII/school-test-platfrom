<?php
/*
 * Copyright 2016 BLCIII
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$db = array(
	'username' => 'quizzer',
	'passwd' => 'testpassword',
	'dbname' => 'quizdb',
);

function initDBConn()
{
	global $db;

	$link = mysqli_connect('localhost',
		$db['username'],
		$db['passwd'],
		$db['dbname']);
	if (!$link) {
		die('MySQL connection error');
	}
	if ($link->set_charset("utf8") == false) {
		die("MySQL charset error\n"
			.$link->error);
	}

	return $link;
}

function callSQLProc(string $query)
{
	global $link;
	require_once('../errors.php');

	if ($link->multi_query('CALL '.$query) == false) {
		fatal_error(__FILE__, __LINE__, $link->error);
	}
	$result = $link->store_result();
	if (!is_bool($result)) {
		$row = $result->fetch_assoc();
		//PHP copies arrays by default
		$retval = $row;
		$result->free();
		//loop through "all" result sets to satisfy SQL protocol
		//(even though there's only one result set)
	} else {
		$retval = $result;
	}
	while($link->next_result());
	return $retval;
}
